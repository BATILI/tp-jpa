package org.batili.serie2.exo4;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
@Entity
public class Musicien implements Serializable {
	@Id @GeneratedValue(strategy= GenerationType.AUTO) 
private int id;
private String nom;

@ManyToMany
private Set<Instrument> instruments=new HashSet<>();

public int getId() {
	return id;
}
public boolean addInstrument(Instrument instrument) {
	return this.instruments.add(instrument);
	
}

public Musicien(String nom) {

	this.nom = nom;
}

public Musicien() {
}
@Override
public String toString() {
	return "Musicien [nom=" + nom + "]";
}
public void setId(int id) {
	this.id = id;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
}
