package org.batili.serie2.exo4;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.batili.serie2.exo4.util.TypeInstrument;
@SuppressWarnings("serial")
@Entity

public class Instrument implements Serializable {
	
	@Id @GeneratedValue(strategy= GenerationType.AUTO) 
	private int id;
	private String nom;
	
	@Enumerated(EnumType.STRING)
	private TypeInstrument type;
	private String typeInstrument;
	
	public String getNom() {
		return nom;
	}
	
	
	public Instrument() {
		
	}


	public Instrument(String nom, TypeInstrument type) {
	
		this.nom = nom;
		this.type = type;
	}

	public Instrument(String nom2, String typeInstrument) {
		// TODO Auto-generated constructor stub
		this.nom=nom2;
		this.setTypeInstrument(typeInstrument);
	}


	public void setNom(String nom) {
		this.nom = nom;
	}
	public TypeInstrument getType() {
		return type;
	}
	public void setType(TypeInstrument type) {
		this.type = type;
	}
	



	@Override
	public String toString() {
		return "Instrument [nom=" + nom + ", typeInstrument=" + typeInstrument + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTypeInstrument() {
		return typeInstrument;
	}


	public void setTypeInstrument(String typeInstrument) {
		this.typeInstrument = typeInstrument;
	}

}
