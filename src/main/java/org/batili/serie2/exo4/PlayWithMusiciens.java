package org.batili.serie2.exo4;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.batili.serie2.exo4.util.TypeInstrument;

public class PlayWithMusiciens {

	public static void main(String[] args) {
		
		Instrument instrument=new Instrument("violoncelle",TypeInstrument.cordes);
		Instrument instrument1=new Instrument("Fl�te",TypeInstrument.vent);
		Instrument instrument2=new Instrument("Clarinette",TypeInstrument.bois);


		System.out.println("Instrument ="+instrument);
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("tp-jpa");
		System.out.println("EMF"+emf);
		EntityManager entityManager=emf.createEntityManager();
		EntityTransaction transaction=entityManager.getTransaction();
		transaction.begin();
		entityManager.persist(instrument);
		entityManager.persist(instrument1);
		entityManager.persist(instrument2);

		transaction.commit();

	}

}
