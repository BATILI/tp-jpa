package org.batili.serie2.exo4;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MusiciensAndInstruments {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		readInstruments();

	}

	private static void readInstruments() {
		Function<String,Instrument>lineToInstrument=
				line -> {
		String[] split= line.split(" ");
		
		String nom=split[0];
		String typeInstrument=split[1];
		Instrument instrument=new Instrument(nom, typeInstrument);
		return instrument;
		

				};
		List <Instrument> instruments=List.of();		
		Path fichierInstruments=Path.of("data/instruments.txt");
		try {
			Stream<String> instrumentsLines=Files.newBufferedReader(fichierInstruments).lines();
			instruments= instrumentsLines
					.skip(0)
					.map(line->lineToInstrument.apply(line))
			.collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		instruments.forEach(System.out::println);
	}

}
